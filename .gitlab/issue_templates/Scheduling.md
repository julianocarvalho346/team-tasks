<!--

Set issue title as "Scheduling issue for the MAJOR.MINOR release" where MAJOR.MINOR is the GitLab version.

This template is used to create a release's scheduling issue. The scheduling issue
is used to raise visibility to the team on what issues are being looked at as
candidates for scheduling, and provides a place for them to offer up scheduling suggestions.

Engineering manager and Product manager are responsible for scheduling
work that is a direct responsibility of the Distribution team.

Typically the Engineering manager is the one to create this issue as part of their scheduling workflow

-->

### Product Priorities
<!--

The product manager should provide a prioritized list of major themes for the release

-->
1. ..

### Deliverable Board

Issues on this board have already been reviewed and scheduled for the upcoming release. Each column represents a priority level. The highest ranked issues for each priority level are at the top of each column.
<!--

Change milestone_title=MAJOR.MINOR

-->
- [Deliverable board](https://gitlab.com/groups/gitlab-org/-/boards/1282058?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Deliverable&label_name[]=group%3A%3Adistribution&milestone_title=MAJOR.MINOR)

### Priority board

Priority board categorizes the importance of all open issues, regardless of milestone. It's useful when looking for
issues to consider for scheduling. Board includes issues from Omnibus, Charts, Team tasks projects.

- [Priority board](https://gitlab.com/groups/gitlab-org/-/boards/1282068?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Adistribution)

### Scheduling board

<!--

The Scheduling boards can be used to organize the issue candidates. The boards should be curated/scoped down by the Engineering Manager
and Product Manager during their regular meetings. Once the lists are agreed upon, the issues will get the milestone assigned

-->  

For Scheduling board is a work board used specifically during the scheduling process to provide an overview of candidates and 
get an idea of the next release before assigning the milestone to issues. Board includes issues from Omnibus, Charts, Team tasks projects.

- [For Scheduling board](https://gitlab.com/groups/gitlab-org/-/boards/1282075?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=For%20Scheduling&label_name[]=group%3A%3Adistribution)

### Status

/cc @gitlab-org/distribution

1. [ ] Collect initial candidate issues
1. [ ] Issues added to the Deliverable board via label
1. [ ] Scheduling boards sorted/arranged by importance
1. [ ] Scheduling boards contain candidates from both Engineering and Product
1. [ ] Scheduling boards scoped to a reasonable amount of work for the release
1. [ ] Managers schedule the agreed issues for the release
